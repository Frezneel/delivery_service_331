package xa.batch331.transaction_customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionCustomerApplication.class, args);
	}

}
