package xa.batch331.transaction_customer.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import xa.batch331.transaction_customer.models.Booking;
import xa.batch331.transaction_customer.models.BookingDTO;
import xa.batch331.transaction_customer.models.Invoice;
import xa.batch331.transaction_customer.repositories.BookingRepo;
import xa.batch331.transaction_customer.repositories.InvoiceRepo;

import java.util.List;
import java.util.Optional;

@Service
public class BookingService {
    private static final Logger logger = LoggerFactory.getLogger(BookingService.class);
    private static final String TOPIC = "booking";

    @Autowired
    private BookingRepo bookingRepo;

    @Autowired
    private InvoiceRepo invoiceRepo;

    @Autowired
    private KafkaTemplate<String, Object>kafkaTemplate;

    public List<Booking> getAllBooking(){
        return this.bookingRepo.findAll();
    }

    //Ambil berdasarkan id
    public Optional<Booking> getBookingById(Long id){
        return this.bookingRepo.findById(id);
    }

    //Menyimpan Booking
    public void saveBooking(Booking booking){
        this.bookingRepo.save(booking);
        //auto add invoice
        Invoice invoice = new Invoice();
        invoice.setBooking_id(booking.getBooking_id());
        this.invoiceRepo.save(invoice);
    }

    //Hapus berdasarkan id
    public void deleteBooking(Long id){
        this.bookingRepo.deleteById(id);
    }

    public void sendKafka(String message){
        logger.info(String.format(">>>>>>>>> Booking message --> %s", message));
        this.kafkaTemplate.send(TOPIC, message);
    }

    public BookingDTO convertToDTO(Booking booking){
        ModelMapper modelMapper = new ModelMapper();
        BookingDTO bookingDTO = modelMapper.map(booking, BookingDTO.class);
        return bookingDTO;
    }


}
