package xa.batch331.transaction_customer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.transaction_customer.models.Customer;
import xa.batch331.transaction_customer.repositories.CustomerRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepo customerRepo;

    //Ambil semua
    public List<Customer> getAllCustomer(){
        return this.customerRepo.findAll();
    }

    //Ambil berdasarkan id
    public Optional<Customer> getCustomerById(Long id){
        return this.customerRepo.findById(id);
    }

    //Menyimpan customer
    public void saveCustomer(Customer customer){
        this.customerRepo.save(customer);
    }

    //Hapus berdasarkan id
    public void deleteCustomer(Long id){
        this.customerRepo.deleteById(id);
    }


}
