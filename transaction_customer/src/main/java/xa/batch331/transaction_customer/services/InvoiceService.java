package xa.batch331.transaction_customer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.transaction_customer.models.Invoice;
import xa.batch331.transaction_customer.repositories.InvoiceRepo;

import java.util.List;
import java.util.Optional;

@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepo invoiceRepo;

    public List<Invoice> getAllInvoice(){
        return this.invoiceRepo.findAll();
    }

    //Ambil berdasarkan id
    public Optional<Invoice> getInvoiceById(Long id){
        return this.invoiceRepo.findById(id);
    }

    //Menyimpan invoice
    public void saveInvoice(Invoice invoice){
        this.invoiceRepo.save(invoice);
    }

    //Hapus berdasarkan id
    public void deleteInvoice(Long id){
        this.invoiceRepo.deleteById(id);
    }

}
