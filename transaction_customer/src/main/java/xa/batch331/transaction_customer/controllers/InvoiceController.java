package xa.batch331.transaction_customer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.transaction_customer.models.Invoice;
import xa.batch331.transaction_customer.services.InvoiceService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/invoice")
    public ResponseEntity<List<Invoice>> getAllInvoice(){
        try {
            List<Invoice> invoices = this.invoiceService.getAllInvoice();
            return new ResponseEntity<>(invoices, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/invoice/{id}")
    public ResponseEntity<Object> getInvoiceById(@PathVariable("id") Long id){
        try {
            Optional<Invoice> invoice = this.invoiceService.getInvoiceById(id);
            if (invoice.isPresent()){
                return new ResponseEntity<>(invoice, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/invoice/{id}")
    public ResponseEntity<Object> editInvoceById(@PathVariable("id") Long id, @RequestBody Invoice invoice){
        try {
            Optional<Invoice> invoice_data = this.invoiceService.getInvoiceById(id);
            if (invoice_data.isPresent()){
                invoice.setInvoice_id(id);
                this.invoiceService.saveInvoice(invoice);
                return new ResponseEntity<>(invoice, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/invoice/{id}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable("id") Long id){
        try {
            Optional<Invoice> invoice = this.invoiceService.getInvoiceById(id);
            if (invoice.isPresent()){
                this.invoiceService.deleteInvoice(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/invoice")
    public ResponseEntity<Object> saveInvoice(@RequestBody Invoice invoice){
        try {
            this.invoiceService.saveInvoice(invoice);
            return new ResponseEntity<>(invoice, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
