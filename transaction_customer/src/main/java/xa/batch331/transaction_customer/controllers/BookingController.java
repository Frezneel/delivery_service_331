package xa.batch331.transaction_customer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.transaction_customer.models.Booking;
import xa.batch331.transaction_customer.models.BookingDTO;
import xa.batch331.transaction_customer.services.BookingService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @GetMapping("/booking")
    public ResponseEntity<List<Booking>> getAllBooking(){
        try {
            List<Booking> bookings = this.bookingService.getAllBooking();
            return new ResponseEntity<>(bookings, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/booking/{id}")
    public ResponseEntity<Object> getBookingById(@PathVariable("id") Long id){
        try {
            Optional<Booking> booking = this.bookingService.getBookingById(id);
            if (booking.isPresent()){
                return new ResponseEntity<>(booking, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/booking")
    public ResponseEntity<Object> editInvoceById(@RequestBody Booking booking){
        ObjectMapper obj = new ObjectMapper();
        try {
            Optional<Booking> booking_data = this.bookingService.getBookingById(booking.getBooking_id());
            if (booking_data.isPresent()){
                this.bookingService.saveBooking(booking);
                // Kalau edit gimana?
                BookingDTO bookingDTO = this.bookingService.convertToDTO(booking);
                String jsonString = obj.writeValueAsString(bookingDTO);
                this.bookingService.sendKafka(jsonString);
                return new ResponseEntity<>(booking, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/booking/{id}")
    public ResponseEntity<?> deleteBookingById(@PathVariable("id") Long id){
        try {
            Optional<Booking> booking = this.bookingService.getBookingById(id);
            if (booking.isPresent()){
                this.bookingService.deleteBooking(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/booking")
    public ResponseEntity<Object> saveBooking(@RequestBody Booking booking){
        ObjectMapper obj = new ObjectMapper();
        try {
            this.bookingService.saveBooking(booking);
            BookingDTO bookingDTO = this.bookingService.convertToDTO(booking);
            String jsonString = obj.writeValueAsString(bookingDTO);
            this.bookingService.sendKafka(jsonString);
            return new ResponseEntity<>(booking, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
