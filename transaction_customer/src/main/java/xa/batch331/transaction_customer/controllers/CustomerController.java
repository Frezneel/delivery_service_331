package xa.batch331.transaction_customer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.transaction_customer.models.Customer;
import xa.batch331.transaction_customer.services.CustomerService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> getAllCustomer(){
        try {
            List<Customer> customers = this.customerService.getAllCustomer();
            return new ResponseEntity<>(customers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/customer/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") Long id){
        try {
            Optional<Customer> customer = this.customerService.getCustomerById(id);
            if (customer.isPresent()){
                return new ResponseEntity<>(customer, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<Object> editCustomerById(@PathVariable("id") Long id, @RequestBody Customer customer){
        try {
            Optional<Customer> customer_data = this.customerService.getCustomerById(id);
            if (customer_data.isPresent()){
                customer.setCust_id(id);
                this.customerService.saveCustomer(customer);
                return new ResponseEntity<>(customer, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable("id") Long id){
        try {
            Optional<Customer> customer = this.customerService.getCustomerById(id);
            if (customer.isPresent()){
                this.customerService.deleteCustomer(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customer")
    public ResponseEntity<Object> saveCustomer(@RequestBody Customer customer){
        try {
            this.customerService.saveCustomer(customer);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
