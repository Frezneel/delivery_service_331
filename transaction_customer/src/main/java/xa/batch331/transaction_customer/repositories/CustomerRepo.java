package xa.batch331.transaction_customer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xa.batch331.transaction_customer.models.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {
}
