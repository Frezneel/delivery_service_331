package xa.batch331.transaction_cargo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionCargoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionCargoApplication.class, args);
	}

}
