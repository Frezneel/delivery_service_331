package xa.batch331.transaction_cargo.models;

import jakarta.persistence.*;

@Entity
@Table(name = "cargo_info")
public class CargoInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cargo_id")
    private Long cargo_id;

    @ManyToOne
    @JoinColumn(name = "booking_id", updatable = false, insertable = false)
    private Booking booking;

    @Column(name = "booking_id")
    private Long booking_id;

    @Column(name = "cargo_type")
    private String cargo_type;

    @Column(name = "cargo_description")
    private String cargo_description;

    @Column(name = "cargo_weight")
    private Double cargo_weight;

    @Column(name = "cargo_volume")
    private Double cargo_volume;

    @Column(name = "hazardous_or_fragile")
    private String hazardous_or_fragile;

    @Column(name = "quantitiy")
    private Long quantitiy;

    @Column(name = "unit_price")
    private Double unit_price;

    @Column(name = "cargo_price")
    private Double cargo_price;

    public Long getCargo_id() {
        return cargo_id;
    }

    public void setCargo_id(Long cargo_id) {
        this.cargo_id = cargo_id;
    }

    public Long getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Long booking_id) {
        this.booking_id = booking_id;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public String getCargo_type() {
        return cargo_type;
    }

    public void setCargo_type(String cargo_type) {
        this.cargo_type = cargo_type;
    }

    public String getCargo_description() {
        return cargo_description;
    }

    public void setCargo_description(String cargo_description) {
        this.cargo_description = cargo_description;
    }

    public Double getCargo_weight() {
        return cargo_weight;
    }

    public void setCargo_weight(Double cargo_weight) {
        this.cargo_weight = cargo_weight;
    }

    public Double getCargo_volume() {
        return cargo_volume;
    }

    public void setCargo_volume(Double cargo_volume) {
        this.cargo_volume = cargo_volume;
    }

    public String getHazardous_or_fragile() {
        return hazardous_or_fragile;
    }

    public void setHazardous_or_fragile(String hazardous_or_fragile) {
        this.hazardous_or_fragile = hazardous_or_fragile;
    }

    public Long getQuantitiy() {
        return quantitiy;
    }

    public void setQuantitiy(Long quantitiy) {
        this.quantitiy = quantitiy;
    }

    public Double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(Double unit_price) {
        this.unit_price = unit_price;
    }

    public Double getCargo_price() {
        return cargo_price;
    }

    public void setCargo_price(Double cargo_price) {
        this.cargo_price = cargo_price;
    }
}
