package xa.batch331.transaction_cargo.models;


import jakarta.persistence.*;

@Entity
@Table(name = "vessel_info")
public class VesselInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vessel_id")
    private Long vessel_id;

    @Column(name = "vessel_name")
    private String vessel_name;

    @Column(name = "vessel_capacity")
    private Long vessel_capacity;

    @Column(name = "vessel_speed")
    private Double vessel_speed;

    @Column(name = "vessel_length")
    private Double vessel_length;

    @Column(name = "vessel_breadth")
    private Double vessel_breadth;

    @Column(name = "vessel_type")
    private String vessel_type;

    @Column(name = "vessel_status")
    private String vessel_status;

    @Column(name = "gross_tonnage")
    private Double gross_tonnage;

    @Column(name = "deadweight_tonnage")
    private Double deadweight_tonnage;

    @ManyToOne
    @JoinColumn(name = "cargo_id", insertable = false, updatable = false)
    private CargoInfo cargoInfo;

    @Column(name = "cargo_id")
    private Long cargo_id;

    public Long getVessel_id() {
        return vessel_id;
    }

    public void setVessel_id(Long vessel_id) {
        this.vessel_id = vessel_id;
    }

    public String getVessel_name() {
        return vessel_name;
    }

    public void setVessel_name(String vessel_name) {
        this.vessel_name = vessel_name;
    }

    public Long getVessel_capacity() {
        return vessel_capacity;
    }

    public void setVessel_capacity(Long vessel_capacity) {
        this.vessel_capacity = vessel_capacity;
    }

    public Double getVessel_speed() {
        return vessel_speed;
    }

    public void setVessel_speed(Double vessel_speed) {
        this.vessel_speed = vessel_speed;
    }

    public Double getVessel_length() {
        return vessel_length;
    }

    public void setVessel_length(Double vessel_length) {
        this.vessel_length = vessel_length;
    }

    public Double getVessel_breadth() {
        return vessel_breadth;
    }

    public void setVessel_breadth(Double vessel_breadth) {
        this.vessel_breadth = vessel_breadth;
    }

    public String getVessel_type() {
        return vessel_type;
    }

    public void setVessel_type(String vessel_type) {
        this.vessel_type = vessel_type;
    }

    public String getVessel_status() {
        return vessel_status;
    }

    public void setVessel_status(String vessel_status) {
        this.vessel_status = vessel_status;
    }

    public Double getGross_tonnage() {
        return gross_tonnage;
    }

    public void setGross_tonnage(Double gross_tonnage) {
        this.gross_tonnage = gross_tonnage;
    }

    public Double getDeadweight_tonnage() {
        return deadweight_tonnage;
    }

    public void setDeadweight_tonnage(Double deadweight_tonnage) {
        this.deadweight_tonnage = deadweight_tonnage;
    }

    public CargoInfo getCargoInfo() {
        return cargoInfo;
    }

    public void setCargoInfo(CargoInfo cargoInfo) {
        this.cargoInfo = cargoInfo;
    }

    public Long getCargo_id() {
        return cargo_id;
    }

    public void setCargo_id(Long cargo_id) {
        this.cargo_id = cargo_id;
    }
}
