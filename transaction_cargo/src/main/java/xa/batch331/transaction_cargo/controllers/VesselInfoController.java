package xa.batch331.transaction_cargo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.transaction_cargo.models.VesselInfo;
import xa.batch331.transaction_cargo.services.VesselInfoService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class VesselInfoController {

    @Autowired
    private VesselInfoService vesselInfoService;

    @GetMapping("/vesselinfo")
    public ResponseEntity<List<VesselInfo>> getAllVesselInfo(){
        try {
            List<VesselInfo> vesselInfos = this.vesselInfoService.getAllVesselInfo();
            return new ResponseEntity<>(vesselInfos, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/vesselinfo/{id}")
    public ResponseEntity<Object> getVesselInfoById(@PathVariable("id") Long id){
        try {
            Optional<VesselInfo> vesselInfo = this.vesselInfoService.getVeselInfoById(id);
            if (vesselInfo.isPresent()){
                return new ResponseEntity<>(vesselInfo, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/vesselinfo/{id}")
    public ResponseEntity<Object> editVesselInfoById(@PathVariable("id") Long id, @RequestBody VesselInfo vesselInfo){
        try {
            Optional<VesselInfo> vesselInfo_data = this.vesselInfoService.getVeselInfoById(id);
            if (vesselInfo_data.isPresent()){
                vesselInfo.setCargo_id(id);
                this.vesselInfoService.saveVeselInfo(vesselInfo);
                return new ResponseEntity<>(vesselInfo, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/vesselinfo/{id}")
    public ResponseEntity<?> deleteVesselInfoById(@PathVariable("id") Long id){
        try {
            Optional<VesselInfo> vesselInfo = this.vesselInfoService.getVeselInfoById(id);
            if (vesselInfo.isPresent()){
                this.vesselInfoService.deleteVeselInfo(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/vesselinfo")
    public ResponseEntity<Object> saveVesselInfo(@RequestBody VesselInfo vesselInfo){
        try {
            this.vesselInfoService.saveVeselInfo(vesselInfo);
            return new ResponseEntity<>(vesselInfo, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
