package xa.batch331.transaction_cargo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.transaction_cargo.models.CargoInfo;
import xa.batch331.transaction_cargo.services.CargoInfoService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class CargoInfoController {

    @Autowired
    private CargoInfoService cargoInfoService;

    @GetMapping("/cargoinfo")
    public ResponseEntity<List<CargoInfo>> getAllCargoInfo(){
        try {
            List<CargoInfo> cargoInfos = this.cargoInfoService.getAllCargoInfo();
            return new ResponseEntity<>(cargoInfos, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/cargoinfo/{id}")
    public ResponseEntity<Object> getCargoInfoById(@PathVariable("id") Long id){
        try {
            Optional<CargoInfo> cargoInfo = this.cargoInfoService.getCargoInfoById(id);
            if (cargoInfo.isPresent()){
                return new ResponseEntity<>(cargoInfo, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/cargoinfo/{id}")
    public ResponseEntity<Object> editCargoInfoById(@PathVariable("id") Long id, @RequestBody CargoInfo cargoInfo){
        try {
            Optional<CargoInfo> cargoInfo_data = this.cargoInfoService.getCargoInfoById(id);
            if (cargoInfo_data.isPresent()){
                cargoInfo.setCargo_id(id);
                this.cargoInfoService.saveCargoInfo(cargoInfo);
                return new ResponseEntity<>(cargoInfo, HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/cargoinfo/{id}")
    public ResponseEntity<?> deleteCargoInfoById(@PathVariable("id") Long id){
        try {
            Optional<CargoInfo> cargoInfo = this.cargoInfoService.getCargoInfoById(id);
            if (cargoInfo.isPresent()){
                this.cargoInfoService.deleteCargoInfo(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/cargoinfo")
    public ResponseEntity<Object> saveCargoInfo(@RequestBody CargoInfo cargoInfo){
        try {
            this.cargoInfoService.saveCargoInfo(cargoInfo);
            return new ResponseEntity<>(cargoInfo, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
