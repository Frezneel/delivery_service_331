package xa.batch331.transaction_cargo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xa.batch331.transaction_cargo.models.VesselInfo;

@Repository
public interface VesselInfoRepo extends JpaRepository<VesselInfo, Long> {
}
