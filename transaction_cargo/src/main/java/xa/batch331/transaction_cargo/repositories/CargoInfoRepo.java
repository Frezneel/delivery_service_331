package xa.batch331.transaction_cargo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xa.batch331.transaction_cargo.models.CargoInfo;

@Repository
public interface CargoInfoRepo extends JpaRepository<CargoInfo, Long> {
}
