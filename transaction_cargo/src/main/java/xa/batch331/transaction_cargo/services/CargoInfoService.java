package xa.batch331.transaction_cargo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.transaction_cargo.models.CargoInfo;
import xa.batch331.transaction_cargo.repositories.CargoInfoRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CargoInfoService {

    @Autowired
    private CargoInfoRepo cargoInfoRepo;

    //Ambil semua
    public List<CargoInfo> getAllCargoInfo(){
        return this.cargoInfoRepo.findAll();
    }

    //Ambil berdasarkan id
    public Optional<CargoInfo> getCargoInfoById(Long id){
        return this.cargoInfoRepo.findById(id);
    }

    //Menyimpan CargoInfo
    public void saveCargoInfo(CargoInfo cargoInfo){
        this.cargoInfoRepo.save(cargoInfo);
    }

    //Hapus berdasarkan id
    public void deleteCargoInfo(Long id){
        this.cargoInfoRepo.deleteById(id);
    }

}
