package xa.batch331.transaction_cargo.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import xa.batch331.transaction_cargo.models.Booking;
import xa.batch331.transaction_cargo.repositories.BookingRepo;

@Service
public class BookingConsumerService {

    private final Logger logger = LoggerFactory.getLogger(BookingConsumerService.class);
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private BookingRepo bookingRepo;

    @KafkaListener(topics = "booking", groupId = "group_booking")
    public void consumeBooking(String message) throws JsonProcessingException {
        Booking booking = objectMapper.readValue(message, Booking.class);
        this.bookingRepo.save(booking);

        logger.info(String.format("<<<<<<<<< Consume Booking -> %s", message));
    }

}
