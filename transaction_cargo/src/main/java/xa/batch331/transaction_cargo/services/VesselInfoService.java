package xa.batch331.transaction_cargo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.transaction_cargo.models.VesselInfo;
import xa.batch331.transaction_cargo.repositories.VesselInfoRepo;

import java.util.List;
import java.util.Optional;

@Service
public class VesselInfoService {

    @Autowired
    private VesselInfoRepo vesselInfoRepo;

    //Ambil semua
    public List<VesselInfo> getAllVesselInfo(){
        return this.vesselInfoRepo.findAll();
    }

    //Ambil berdasarkan id
    public Optional<VesselInfo> getVeselInfoById(Long id){
        return this.vesselInfoRepo.findById(id);
    }

    //Menyimpan VesselInfo
    public void saveVeselInfo(VesselInfo vesselInfo){
        this.vesselInfoRepo.save(vesselInfo);
    }

    //Hapus berdasarkan id
    public void deleteVeselInfo(Long id){
        this.vesselInfoRepo.deleteById(id);
    }

}
